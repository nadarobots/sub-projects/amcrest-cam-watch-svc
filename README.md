# amcrest-cam-watch-svc

Description
* Watches NFS directory for motion captures from an Amcrest IP CAM.
* Renames and moves the captures to a new flat directory.

TODO
* Cleanup old captures
* Cleanup old folder structures

<br />

## Getting Started

Docker
```
docker run -d --name camwatch -v nfs:/exports --privileged docker.io/nadarobots/camwatch:latest
```

nfs
```
docker run -d --name nfs -v nfs:/exports --privileged --net=host docker.io/nadarobots/nfs-server:latest
```

samba
```
docker run -it --name samba --net=host -v nfs:/mount -d dperson/samba -p -s "garagecam;/mount/garagecam;yes;no;yes"
```

ftp
```
docker run -d -p 21:21 -p 21000-21010:21000-21010 -e USERS="one|1234" -e ADDRESS=192.50.10.30 -v nfs:/ftp/one --name ftp delfer/alpine-ftp-server
```

httpd
```
docker run -dit --name httpd -p 8080:80 -v nfs:/usr/local/apache2/htdocs httpd:2.4
```







Script
```
cp nada_watch.sh /usr/bin/
chmod +x /usr/bin/nada_watch.sh
```

Service
```
cp nada_watch.service /etc/systemd/system/nada_watch.service
chmod 755 /etc/systemd/system/nada_watch.service
```

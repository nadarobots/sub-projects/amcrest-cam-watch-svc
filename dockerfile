FROM ubuntu

ENV DEBIAN_FRONTEND noninteractive

ADD nada_watch.sh /usr/bin

RUN apt-get update -qq && \
    apt-get install -y inotify-tools curl -qq && \
    chmod +x /usr/bin/nada_watch.sh

CMD bash /usr/bin/nada_watch.sh


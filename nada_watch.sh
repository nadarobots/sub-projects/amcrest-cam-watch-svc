#!/bin/bash
### BEGIN INIT INFO
# Provides:          nada-watch
# Required-Start:    $all
# Required-Stop:
# Default-Start:     2 3 4 5
# Default-Stop:
# Short-Description: watch dir for images from amcrest camera and mv/rename them.
### END INIT INFO

rootdir="/exports"
watchdir="${rootdir}/garagecam/AMC0462F2F9D9578F9"
regex="^(.+)\/([^\/]+)\/([^\/]+)\/([^\/]+)\/([^\/]+)\/([^\/]+)\/([0-9][0-9])([^\/*]+)$"

# Prep Logging
mkdir -p /var/log/nada_watch
touch /var/log/nada_watch/watch.log

inotifywait -m -e create -r $watchdir -e moved_to |
    while read directory action file; do
        chmod 777 -R $watchdir
        if [[ "$file" =~ .*jpg$ ]]; then # Does the file end with .xml?
            fullfile=${directory}${file}
            echo "Screenshot Detected ${fullfile}" | tee -a /var/log/nada_watch/watch.log
            if [[ $fullfile =~ $regex ]]
            then
                echo "Moving and Renaming ${file} to ${BASH_REMATCH[2]}-${BASH_REMATCH[5]}-${BASH_REMATCH[6]}-${BASH_REMATCH[7]}.jpg" | tee -a /var/log/nada_watch/watch.log
                mv "${directory}${file}" "${rootdir}/aiwatch/${BASH_REMATCH[2]}-${BASH_REMATCH[5]}-${BASH_REMATCH[6]}-${BASH_REMATCH[7]}.jpg"
                curl http://192.50.10.34:1880/endpoint/doods?url=${BASH_REMATCH[2]}-${BASH_REMATCH[5]}-${BASH_REMATCH[6]}-${BASH_REMATCH[7]}
                echo "Cleaning up Dir ${rootdir}/aiwatch/${BASH_REMATCH[2]}" | tee -a /var/log/nada_watch/watch.log
                rm -rf "${watchdir}/${BASH_REMATCH[2]}"
            else
                echo "${fullfile} failed to match regex." | tee -a /var/log/nada_watch/watch.log
            fi
        fi
    done
